# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  delete_lowercase(str.split('')).join
end

def delete_lowercase(arr)
  arr.delete_if { |el| el >= 'a' && el <= 'z' }
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_index = (str.length - 1) / 2.0
  str[mid_index.floor..mid_index.ceil]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count VOWELS.join
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce { |product, el| el * product }
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ''
  arr.each { |el| string << el << separator }
  separator == '' ? string : string[0...-1]
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = ''
  str.chars.each_index do |i|
    weird_str += i.odd? ? str[i].upcase : str[i].downcase
  end
  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reverse_words_longer_than_4(str.split).join(' ')
end

def reverse_words_longer_than_4(arr_of_words)
  arr_of_words.map { |word| word.length > 4 ? word.reverse : word }
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map do |el|
    str = ''
    str << 'fizz' if el % 3 == 0
    str << 'buzz' if el % 5 == 0
    str.empty? ? el : str
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  rev_arr = []
  arr.each { |el| rev_arr.unshift(el) }
  rev_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (1..num).to_a.count { |el| num % el == 0 } <= 2
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).to_a.select { |el| num % el == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |el| prime?(el) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even, odd = [], []
  arr.each { |el| el % 2 == 0 ? even.push(el) : odd.push(el) }
  even.length > odd.length ? odd[0] : even[0]
end
